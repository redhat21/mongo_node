const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const crypto = require('crypto');
const mongoose = require('mongoose');
const multer = require('multer');
const GridFsStorage = require('multer-gridfs-storage');
const Grid = require('gridfs-stream');
const methodOverride = require('method-override');

var cors = require('cors');

const audiobooks = require('./audiobooks');

const app = express();

// MiddleWare
app.use(bodyParser.json());
app.use(methodOverride('_method'));
app.use(cors());

app.set('view engine', 'ejs');


// Mongo URI
const mongoURI = 'mongodb://localhost:27017/mongouploads';


// Create mongo connection
const conn = mongoose.createConnection(mongoURI);



// init gfs
let gfs;

conn.once('open', () => {
    //intialize stream
    gfs = Grid(conn.db, mongoose.mongo);
    gfs.collection('uploads');
})

// create storage engine

const storage = new GridFsStorage({
    url: mongoURI,
    file: (req, file) => {
        return new Promise((resolve, reject) => {
            crypto.randomBytes(16, (err, buf) => {
                if(err) {
                    return reject(err);
                }
                const filename = buf.toString('hex') + path.extname(file.originalname);
                const fileInfo = {
                    filename: filename,
                    metadata: {
                        title: 'Call of The Wild',
                        author: 'Jack London',
                        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTD1I-7yj6ffyLOCe9f8aJbVPaIAj1poD5mLg&usqp=CAU',
                        language: 'english',
                        genre: 'adventure',
                        published: '1903'
                    },
                    bucketName: 'uploads'
                };
                resolve(fileInfo);
            });
        });
    }
});
const upload = multer({ storage });

// @route GET /
// @desc Loads form 

app.get('/', (req,res) => {
    res.render('index');
}); 

// @route POST /upload
// @desc Uploads file to DB

app.post('/upload', upload.single('file'), (req, res) => {
    res.json({file: req.file});
    //res.redirect('/');
});

// @route GET /files
// @desc Display all files in JSON

app.get('/files', (req,res) => {
    gfs.files.find().toArray((err, files) => {
        // check if files
        if(!files || files.length === 0) 
        {
            return res.status(404).json({
                err: 'No files exist'
            });
        }

        // files exist
        return res.json(files);
    });
});

// @route GET /files/:filename
// @desc Display single file

app.get('/files/:filename', (req,res) => {
    gfs.files.findOne({filename: req.params.filename}, (err, file) => {
        // check if file
        if(!file || file.length === 0)
        {
            return res.status(404).json({
                err: 'No file exists'
            });
        }
        // file exists
        return res.json(file);
    });
});

// @route GET /image/:filename
// @desc Display image

app.get('/image/:filename', (req,res) => {
    gfs.files.findOne({filename: req.params.filename}, (err, file) => {
        // check if file
        if(!file || file.length === 0)
        {
            return res.status(404).json({
                err: 'No file exists'
            });
        }
        // check if image
        if(file.contentType === 'image/jpeg' || file.contentType === 'image/png')
        {
            // read output to browser
            const readstream = gfs.createReadStream(file.filename);
            readstream.pipe(res);
        }
        else 
        {
            res.status(404).json({
                err: 'Not an image'
            });
        }
    });
});

app.get('/files/audio/:filename', (req,res) => {
    gfs.files.findOne({filename: req.params.filename}, (err, file) => {
        // check if file
        if(!file || file.length === 0)
        {
            return res.status(404).json({
                err: 'No file exists'
            });
        }
        // check if audio
        if(file.contentType === 'audio/mpeg')
        {
            // read output to browser
            const readstream = gfs.createReadStream(file.filename);
            readstream.pipe(res);
        }
        else 
        {
            res.status(404).json({
                err: 'Not an audio file'
            });
        }
    });
});

app.get('/files/audio/metadata/title/:title', (req,res) => {
    gfs.files.find((err, file) => {

        // check if file
        if(!file || file.length === 0)
        {
            return res.status(404).json({
                err: 'No file exists'
            });
        }
        // check if audio
        if(file.contentType === 'audio/mpeg' && file.metadata.title === req.params.title)
        {
            // read output to browser
            const readstream = gfs.createReadStream(file.filename);
            readstream.pipe(res);
        }
        else 
        {
            res.status(404).json({
                err: 'Not an audio file'
            });
        }
    });
});

app.get('/files/audio/metadata/author/:author', (req,res) => {
    gfs.files.find((err, file) => {

        // check if file
        if(!file || file.length === 0)
        {
            return res.status(404).json({
                err: 'No file exists'
            });
        }
        // check if audio
        if(file.contentType === 'audio/mpeg' && file.metadata.author === req.params.author)
        {
            // read output to browser
            const readstream = gfs.createReadStream(file.filename);
            readstream.pipe(res);
        }
        else 
        {
            res.status(404).json({
                err: 'Not an audio file'
            });
        }
    });
});

const port = 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));