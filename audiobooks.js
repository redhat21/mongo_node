const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const audiobookSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required:true
    },
    isbn: {
        type: Number,
        required: true,
        unique: true
    },
    image: {
        type: String
    },
    description: {
        type: String
    },
    audio: {
        type: String,
        required: true
    }
});

var audiobooks = mongoose.model('audiobook', audiobookSchema);

module.exports = audiobooks;